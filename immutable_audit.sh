#!/bin/bash

# Define the file path
RULES_FILE="/etc/audit/rules.d/99-finalize.rules"

# Check if the file exists
if [ ! -f "$RULES_FILE" ]; then
    # File does not exist, create it and append the rule
    echo "-e 2" >> "$RULES_FILE"
    echo "File $RULES_FILE did not exist. Rule '-e 2' has been created and added."
else
    # File exists, do not append anything
    echo "File $RULES_FILE already exists. No changes were made."
fi
