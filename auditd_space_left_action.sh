#!/bin/bash

# Define the configuration file path
CONF_FILE="/etc/audit/auditd.conf"

# Define an associative array for the parameters and their desired values
declare -A params
params=(
    [space_left_action]="email"
    [action_mail_acct]="root"
    [admin_space_left_action]="halt"
)

# Backup the original file before making changes
cp "$CONF_FILE" "${CONF_FILE}.bak"

# Iterate over the associative array
for PARAM in "${!params[@]}"; do
    VALUE=${params[$PARAM]}
    # Check if the parameter exists in the file
    if grep -q "^${PARAM} = " "$CONF_FILE"; then
        # Parameter exists, update its value
        sed -i "s/^${PARAM} = .*/${PARAM} = ${VALUE}/" "$CONF_FILE"
        echo "Updated $PARAM to $VALUE in $CONF_FILE."
    else
        # Parameter does not exist, append it to the file
        echo "$PARAM = $VALUE" >> "$CONF_FILE"
        echo "Appended $PARAM = $VALUE to $CONF_FILE."
    fi
done

service auditd restart