#!/bin/bash

# Path to the SSHD configuration file
SSHD_CONFIG="/etc/ssh/sshd_config"

# Backup the original SSHD configuration file
cp ${SSHD_CONFIG} ${SSHD_CONFIG}.bak

# Check if AllowTcpForwarding directive exists
if grep -q "^AllowTcpForwarding" ${SSHD_CONFIG}; then
    # AllowTcpForwarding directive exists, modify its value to no
    sed -i 's/^AllowTcpForwarding.*/AllowTcpForwarding no/' ${SSHD_CONFIG}
else
    # AllowTcpForwarding directive does not exist, add it to the file
    echo "AllowTcpForwarding no" >> ${SSHD_CONFIG}
fi

# Check if MaxSessions directive exists
if grep -q "^MaxSessions" ${SSHD_CONFIG}; then
    # MaxSessions directive exists, modify its value to no
    sed -i 's/^MaxSessions.*/MaxSessions 10/' ${SSHD_CONFIG}
else
    # MaxSessions directive does not exist, add it to the file
    echo "MaxSessions 10" >> ${SSHD_CONFIG}
fi

# Check if MaxStartups directive exists
if grep -q "^MaxStartups" ${SSHD_CONFIG}; then
    # MaxStartups directive exists, modify its value to no
    sed -i 's/^MaxStartups.*/MaxStartups 10:30:60/' ${SSHD_CONFIG}
else
    # MaxStartups directive does not exist, add it to the file
    echo "MaxStartups 10:30:60" >> ${SSHD_CONFIG}
fi

# # Check if UsePAM directive exists
# if grep -q "^UsePAM" ${SSHD_CONFIG}; then
#     # UsePAM directive exists, modify its value to no
#     sed -i 's/^UsePAM.*/UsePAM yes/' ${SSHD_CONFIG}
# else
#     # UsePAM directive does not exist, add it to the file
#     echo "UsePAM yes" >> ${SSHD_CONFIG}
# fi

# Check if Ciphers directive exists
if grep -q "^Ciphers" ${SSHD_CONFIG}; then
    # Ciphers directive exists, modify its value to no
    sed -i 's/^Ciphers.*/Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr/' ${SSHD_CONFIG}
else
    # Ciphers directive does not exist, add it to the file
    echo "Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr" >> ${SSHD_CONFIG}
fi

# Check if MACs directive exists
if grep -q "^MACs" ${SSHD_CONFIG}; then
    # MACs directive exists, modify its value to no
    sed -i 's/^MACs.*/MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-sha2-512,hmac-sha2-256/' ${SSHD_CONFIG}
else
    # MACs directive does not exist, add it to the file
    echo "MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-sha2-512,hmac-sha2-256" >> ${SSHD_CONFIG}
fi

# Check if KexAlgorithms directive exists
if grep -q "^KexAlgorithms" ${SSHD_CONFIG}; then
    # KexAlgorithms directive exists, modify its value to no
    sed -i 's/^KexAlgorithms.*/KexAlgorithms curve25519-sha256,curve25519-sha256@libssh.org,ecdh-sha2-nistp521,ecdh-sha2-nistp384,ecdh-sha2-nistp256,diffie-hellman-group-exchange-sha256/' ${SSHD_CONFIG}
else
    # KexAlgorithms directive does not exist, add it to the file
    echo "KexAlgorithms curve25519-sha256,curve25519-sha256@libssh.org,ecdh-sha2-nistp521,ecdh-sha2-nistp384,ecdh-sha2-nistp256,diffie-hellman-group-exchange-sha256" >> ${SSHD_CONFIG}
fi

# Restart the SSH service to apply changes
# The command may differ based on your system's init system
# Uncomment the one that applies to your system

# For systems using systemd (e.g., Ubuntu, CentOS 7 and later)
systemctl restart sshd

# For systems using the older SysVinit (e.g., CentOS 6)
# service sshd restart

