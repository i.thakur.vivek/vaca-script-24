#! /bin/bash

cd /etc/kubernetes/kubelet
sudo sed -i '$s/}/,\n"streamingConnectionIdleTimeout":"5h"}/' kubelet-config.json
sudo sed -i '$s/}/,\n"makeIPTablesUtilChains":"true"}/' kubelet-config.json
sudo sed -i '$s/}/,\n"eventRecordQPS":"5"}/' kubelet-config.json
