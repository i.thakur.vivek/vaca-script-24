#!/bin/bash

# Define the paths to the files and their backups
FILES=("/etc/motd" "/etc/issue" "/etc/issue.net")
BACKUP_SUFFIX=".bak"

# Message to write into the files
MESSAGE="All activities performed on this system will be monitored"

# Function to backup and update a file
backup_and_update() {
  local file=$1
  local backup_file="${file}${BACKUP_SUFFIX}"

  # Check if the file exists
  if [ -f "$file" ]; then
    # Backup the existing file
    cp "$file" "$backup_file"
    echo "Backup of $file created at $backup_file"
  else
    echo "No existing $file found. A new one will be created."
  fi

  # Write the new message to the file
  echo "$MESSAGE" > "$file"
  echo "New message written to $file"

  # Verify the contents of the file
  echo "Contents of $file:"
  cat "$file"
}

# Iterate over the files and perform the backup and update
for file in "${FILES[@]}"; do
  backup_and_update "$file"
done
