#!/bin/bash

# Define the cron command
cron_command="0 5 * * * /usr/sbin/aide --check"

# Install the cron job
sudo crontab -u root - <<EOF
$(sudo crontab -u root -l ; echo "$cron_command")
EOF

echo "Cron job added successfully."
