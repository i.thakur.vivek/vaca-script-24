#!/bin/bash

# Check if the script is being run as root
if [ "$(id -u)" -ne 0 ]; then
    echo "This script must be run as root. Exiting..."
    exit 1
fi

echo "Starting system update..."
yum update -y

if [ $? -eq 0 ]; then
    echo "System update completed successfully."
else
    echo "Error occurred during system update. Please check the logs."
fi
