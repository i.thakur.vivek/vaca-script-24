#!/bin/bash

# Define the grub configuration file paths
GRUB_DEFAULT="/etc/default/grub"
GRUB_CFG="/boot/grub2/grub.cfg"

# Define the parameters to add
PARAMS_TO_ADD="audit=1 audit_backlog_limit=8192 ipv6.disable=1"

# Read the current GRUB_CMDLINE_LINUX value, ignoring lines that are commented out
CURRENT_CMDLINE=$(grep "^GRUB_CMDLINE_LINUX=" "$GRUB_DEFAULT" | tail -1)

# Function to check if a parameter is in the current GRUB_CMDLINE_LINUX
containsParam() {
    [[ "$CURRENT_CMDLINE" =~ $1 ]] && return 0 || return 1
}

# Loop through each parameter to check if it's already included
for PARAM in $PARAMS_TO_ADD; do
    if ! containsParam "$PARAM"; then
        # If any parameter is missing, we'll append all parameters to ensure consistency
        NEEDS_UPDATE=true
        break
    fi
done

if [ "$NEEDS_UPDATE" = true ]; then
    echo "Updating GRUB_CMDLINE_LINUX in $GRUB_DEFAULT"
    # Use sed to either append or update the line
    if [[ -z "$CURRENT_CMDLINE" ]]; then
        # If GRUB_CMDLINE_LINUX line does not exist, add it
        echo "GRUB_CMDLINE_LINUX=\"$PARAMS_TO_ADD\"" >> "$GRUB_DEFAULT"
    else
        # If the line exists, replace it with the new parameters, preserving any existing ones not explicitly set here
        sed -i "/^GRUB_CMDLINE_LINUX=/c\GRUB_CMDLINE_LINUX=\"$CURRENT_CMDLINE $PARAMS_TO_ADD\"" "$GRUB_DEFAULT"
    fi
else
    echo "Both audit=1 and audit_backlog_limit=8192 are already set in GRUB_CMDLINE_LINUX in $GRUB_DEFAULT"
fi

# Update GRUB configuration
echo "Updating GRUB configuration..."
grub2-mkconfig -o "$GRUB_CFG"

echo "GRUB configuration has been updated."