#!/bin/bash

# Define packages to update
PACKAGES=(
  rsync
  ncurses
  libfastjson
  tar
  mariadb
  glib2
  screen
  libwebp
  pcre
  sysstat
)

# Update packages
for pkg in "${PACKAGES[@]}"; do
  echo "Updating $pkg..."
  sudo yum update "$pkg" -y
done

# Install gcc if not already installed
if ! command -v gcc &> /dev/null; then
    echo "gcc could not be found. Installing gcc..."
    sudo yum install gcc -y
else
    echo "gcc is already installed."
fi

# Install libwebp from source
WEBP_VERSION="1.3.2"
WEBP_DIR="libwebp-$WEBP_VERSION"

if ! command -v webpinfo &> /dev/null; then
    echo "libwebp is not installed or is not in the PATH. Installing libwebp $WEBP_VERSION from source..."
    sudo wget https://storage.googleapis.com/downloads.webmproject.org/releases/webp/$WEBP_DIR.tar.gz
    sudo tar xvzf $WEBP_DIR.tar.gz
    cd $WEBP_DIR
    sudo ./configure
    sudo make
    sudo make install
    sudo ldconfig
    echo "libwebp $WEBP_VERSION installation completed."
else
    echo "libwebp is already installed."
fi

# Check libwebp version
webpinfo -version