#!/bin/bash

# Function to configure a module
configure_module() {
    local module="$1"
    local filename="/etc/modprobe.d/${module}.conf"

    # Check if the file exists
    if [ -f "$filename" ]; then
        echo "The file $filename already exists, editing it..."
    else
        echo "The file $filename doesn't exist, creating it..."
        touch "$filename"
    fi

    # Add the line to the file
    echo "install $module /bin/true" >> "$filename"

    # Unload the module
    rmmod "$module"

    echo "Done!"

    # Checker
    /usr/sbin/modprobe -n -v "$module"
}

# Configure squashfs module
configure_module "squashfs"

# Configure dccp module
configure_module "dccp"

# Configure sctp module
configure_module "sctp"
