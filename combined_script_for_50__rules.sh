#!/bin/bash

# Function to ensure a file exists and append rules
ensure_and_append_rules() {
    local target_file="$1"
    local rules="$2"
    local keyword="$3"

    # Ensure the file exists
    if [ ! -f "$target_file" ]; then
        echo "File $target_file does not exist. Creating it..."
        touch "$target_file"
    fi

    # Append each rule if not already present
    while IFS= read -r line; do
        if ! grep -Fxq -- "$line" "$target_file"; then
            echo "Appending rule to $target_file: $line"
            echo "$line" | sudo tee -a "$target_file" > /dev/null
        else
            echo "Rule already exists: $line"
        fi
    done <<< "$rules"

    # Restart auditd service to apply changes
    echo "Restarting auditd service..."
    sudo service auditd restart

    # Check for rules to confirm
    echo "Current audit rules for '-k $keyword':"
    /sbin/auditctl -l | /bin/grep -w "$keyword"
}

# Define the target files and audit rules
TARGET_FILE_1="/etc/audit/rules.d/50-access.rules"
RULES_1="-a always,exit -F arch=b64 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EACCES -F auid>=1000 -F auid!=4294967295 -k access
-a always,exit -F arch=b32 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EACCES -F auid>=1000 -F auid!=4294967295 -k access
-a always,exit -F arch=b64 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EPERM -F auid>=1000 -F auid!=4294967295 -k access
-a always,exit -F arch=b32 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EPERM -F auid>=1000 -F auid!=4294967295 -k access"

TARGET_FILE_2="/etc/audit/rules.d/50-mounts.rules"
RULES_2="-a always,exit -F arch=b64 -S mount -F auid>=1000 -F auid!=4294967295 -k mounts
-a always,exit -F arch=b32 -S mount -F auid>=1000 -F auid!=4294967295 -k mounts"

TARGET_FILE_3="/etc/audit/rules.d/50-deletion.rules"
RULES_3="-a always,exit -F arch=b64 -S unlink -S unlinkat -S rename -S renameat -F auid>=1000 -F auid!=4294967295 -k delete
-a always,exit -F arch=b32 -S unlink -S unlinkat -S rename -S renameat -F auid>=1000 -F auid!=4294967295 -k delete"

TARGET_FILE_4="/etc/audit/rules.d/50-scope.rules"
RULES_4="-w /etc/sudoers -p wa -k scope
-w /etc/sudoers.d/ -p wa -k scope"

TARGET_FILE_5="/etc/audit/rules.d/50-actions.rules"
RULES_5="-a always,exit -F arch=b64 -C euid!=uid -F euid=0 -F auid>=1000 -F auid!=4294967295 -S execve -k actions
-a always,exit -F arch=b32 -C euid!=uid -F euid=0 -F auid>=1000 -F auid!=4294967295 -S execve -k actions"

TARGET_FILE_6="/etc/audit/rules.d/50-modules.rules"
RULES_6="-w /sbin/insmod -p x -k modules
-w /sbin/rmmod -p x -k modules
-w /sbin/modprobe -p x -k modules
-a always,exit -F arch=b64 -S init_module -S delete_module -k modules"

TARGET_FILE_7="/etc/audit/rules.d/50-time_change.rules"
RULES_7="-a always,exit -F arch=b64 -S adjtimex -S settimeofday -k time-change
-a always,exit -F arch=b32 -S adjtimex -S settimeofday -S stime -k time-change
-a always,exit -F arch=b64 -S clock_settime -k time-change
-a always,exit -F arch=b32 -S clock_settime -k time-change
-w /etc/localtime -p wa -k time-change"

TARGET_FILE_8="/etc/audit/rules.d/50-identity.rules"
RULES_8="-w /etc/group -p wa -k identity
-w /etc/passwd -p wa -k identity
-w /etc/gshadow -p wa -k identity
-w /etc/shadow -p wa -k identity
-w /etc/security/opasswd -p wa -k identity"

TARGET_FILE_9="/etc/audit/rules.d/50-system_local.rules"
RULES_9="-a always,exit -F arch=b64 -S sethostname -S setdomainname -k system-locale
-a always,exit -F arch=b32 -S sethostname -S setdomainname -k system-locale
-w /etc/issue -p wa -k system-locale
-w /etc/issue.net -p wa -k system-locale
-w /etc/hosts -p wa -k system-locale
-w /etc/sysconfig/network -p wa -k system-locale"

TARGET_FILE_10="/etc/audit/rules.d/50-MAC_policy.rules"
RULES_10="-w /etc/selinux/ -p wa -k MAC-policy
-w /usr/share/selinux/ -p wa -k MAC-policy"

TARGET_FILE_11="/etc/audit/rules.d/50-logins.rules"
RULES_11="-w /var/log/lastlog -p wa -k logins
-w /var/run/faillock/ -p wa -k logins"

TARGET_FILE_12="/etc/audit/rules.d/50-session.rules"
RULES_12="-w /var/run/utmp -p wa -k session
-w /var/log/wtmp -p wa -k logins
-w /var/log/btmp -p wa -k logins"

TARGET_FILE_13="/etc/audit/rules.d/50-perm_mod.rules"
RULES_13="-a always,exit -F arch=b64 -S chmod -S fchmod -S fchmodat -F auid>=1000 -F auid!=4294967295 -k perm_mod
-a always,exit -F arch=b32 -S chmod -S fchmod -S fchmodat -F auid>=1000 -F auid!=4294967295 -k perm_mod
-a always,exit -F arch=b64 -S chown -S fchown -S fchownat -S lchown -F auid>=1000 -F auid!=4294967295 -k perm_mod
-a always,exit -F arch=b32 -S chown -S fchown -S fchownat -S lchown -F auid>=1000 -F auid!=4294967295 -k perm_mod
-a always,exit -F arch=b64 -S setxattr -S lsetxattr -S fsetxattr -S removexattr -S lremovexattr -S fremovexattr -F auid>=1000 -F auid!=4294967295 -k perm_mod
-a always,exit -F arch=b32 -S setxattr -S lsetxattr -S fsetxattr -S removexattr -S lremovexattr -S fremovexattr -F auid>=1000 -F auid!=4294967295 -k perm_mod"

# Ensure and append rules for each file
ensure_and_append_rules "$TARGET_FILE_1" "$RULES_1" "access"
ensure_and_append_rules "$TARGET_FILE_2" "$RULES_2" "mounts"
ensure_and_append_rules "$TARGET_FILE_3" "$RULES_3" "delete"
ensure_and_append_rules "$TARGET_FILE_4" "$RULES_4" "scope"
ensure_and_append_rules "$TARGET_FILE_5" "$RULES_5" "actions"
ensure_and_append_rules "$TARGET_FILE_6" "$RULES_6" "modules"
ensure_and_append_rules "$TARGET_FILE_7" "$RULES_7" "time-change"
ensure_and_append_rules "$TARGET_FILE_8" "$RULES_8" "identity"
ensure_and_append_rules "$TARGET_FILE_9" "$RULES_9" "system-locale"
ensure_and_append_rules "$TARGET_FILE_10" "$RULES_10" "MAC-policy"
ensure_and_append_rules "$TARGET_FILE_11" "$RULES_11" "logins"
ensure_and_append_rules "$TARGET_FILE_12" "$RULES_12" "session"
ensure_and_append_rules "$TARGET_FILE_13" "$RULES_13" "perm_mod"
