#!/bin/bash

# Set the umask value to 027 in /etc/profile.d/*.sh files
for file in /etc/profile.d/*.sh; do
    if [ -f "$file" ] && grep -q 'umask' "$file"; then
        sed -i 's/umask\s*[0-7]\{3\}/umask 027/g' "$file"
    fi
done

# Replace umask 002 with umask 027 in /etc/profile
sed -i 's/umask 002/umask 027/g' /etc/profile
sed -i 's/umask 022/umask 027/g' /etc/profile

# Replace umask 002 with umask 027 in /etc/bashrc
sed -i 's/umask 002/umask 027/g' /etc/bashrc
sed -i 's/umask 022/umask 027/g' /etc/bashrc
