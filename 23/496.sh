#!/bin/bash

# Set the parameters in /etc/sysctl.conf or a /etc/sysctl.d/* file
echo "net.ipv4.conf.all.send_redirects = 0" | tee -a /etc/sysctl.conf >/dev/null
echo "net.ipv4.conf.default.send_redirects = 0" | tee -a /etc/sysctl.conf >/dev/null
echo "net.ipv4.conf.all.accept_source_route = 0" | tee -a /etc/sysctl.conf >/dev/null
echo "net.ipv4.conf.default.accept_source_route = 0" | tee -a /etc/sysctl.conf >/dev/null

# Apply the changes
sysctl -p >/dev/null

# Check if the net.ipv4.conf.default.send_redirects parameter is set to 0
if /bin/grep -s -P '^[\s]*net\.ipv4\.conf\.default\.send_redirects[\s]*=[\s]*0[\s]*$' /etc/sysctl.conf /etc/sysctl.d/* >/dev/null; then
    echo "pass"
else
    echo "fail"
fi

# Set the IPv6 parameters if IPv6 is enabled
ipv6_enabled=$(grep -q "^\s*net\.ipv6\.conf\.all\.disable_ipv6\s*=\s*1" /etc/sysctl.conf /etc/sysctl.d/* 2>/dev/null)
if [ -z "$ipv6_enabled" ]; then
    echo "net.ipv6.conf.all.accept_source_route = 0" | tee -a /etc/sysctl.conf >/dev/null
    echo "net.ipv6.conf.default.accept_source_route = 0" | tee -a /etc/sysctl.conf >/dev/null

    # Apply the changes
    sysctl -p >/dev/null

    # Check if the net.ipv6.conf.default.accept_source_route parameter is set to 0
    if /bin/grep -s -P '^[\s]*net\.ipv6\.conf\.default\.accept_source_route[\s]*=[\s]*0[\s]*$' /etc/sysctl.conf /etc/sysctl.d/* >/dev/null; then
        echo "pass"
    else
        echo "fail"
    fi
fi

# Flush the routing table cache
sysctl -w net.ipv4.route.flush=1
sysctl -w net.ipv6.route.flush=1

## covers 496, 497, 498, 499, 500, 501, 502, 503, 
