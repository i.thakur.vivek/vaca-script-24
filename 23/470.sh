#!/bin/bash

# Install AIDE
yum install -y aide

# Configure AIDE
# (Add configuration steps here)

# Initialize AIDE
aide --init

# Move the database file
mv /var/lib/aide/aide.db.new.gz /var/lib/aide/aide.db.gz

# Echo message when the script completes
echo "AIDE installation and initialization completed successfully."