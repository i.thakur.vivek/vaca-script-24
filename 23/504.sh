#!/bin/bash

# Set IPv4 parameters in /etc/sysctl.conf or a /etc/sysctl.d/* file
echo "net.ipv4.conf.all.accept_redirects = 0" | tee -a /etc/sysctl.conf
echo "net.ipv4.conf.default.accept_redirects = 0" | tee -a /etc/sysctl.conf
echo "net.ipv4.conf.all.secure_redirects = 0" | tee -a /etc/sysctl.conf
echo "net.ipv4.conf.default.secure_redirects = 0" | tee -a /etc/sysctl.conf

# Set the active IPv4 kernel parameters
sysctl -w net.ipv4.conf.all.accept_redirects=0
sysctl -w net.ipv4.conf.default.accept_redirects=0
sysctl -w net.ipv4.conf.all.secure_redirects=0
sysctl -w net.ipv4.conf.default.secure_redirects=0
sysctl -w net.ipv4.route.flush=1

# Check if the IPv4 parameters are set correctly
if grep -qsP '^[\s]*net\.ipv4\.conf\.all\.accept_redirects[\s]*=[\s]*0[\s]*$' /etc/sysctl.conf /etc/sysctl.d/* && \
   grep -qsP '^[\s]*net\.ipv4\.conf\.default\.accept_redirects[\s]*=[\s]*0[\s]*$' /etc/sysctl.conf /etc/sysctl.d/* && \
   grep -qsP '^[\s]*net\.ipv4\.conf\.all\.secure_redirects[\s]*=[\s]*0[\s]*$' /etc/sysctl.conf /etc/sysctl.d/* && \
   grep -qsP '^[\s]*net\.ipv4\.conf\.default\.secure_redirects[\s]*=[\s]*0[\s]*$' /etc/sysctl.conf /etc/sysctl.d/*; then
  echo "IPv4 parameters set correctly."
else
  echo "Failed to set IPv4 parameters."
fi

# Check if IPv6 is disabled
ipv6_disabled=$(sysctl net.ipv6.conf.all.disable_ipv6 | awk '{print $NF}')
if [ "$ipv6_disabled" -eq 1 ]; then
  echo "IPv6 is disabled."
else
  # Set IPv6 parameters in /etc/sysctl.conf or a /etc/sysctl.d/* file
  echo "net.ipv6.conf.all.accept_redirects = 0" | tee -a /etc/sysctl.conf
  echo "net.ipv6.conf.default.accept_redirects = 0" | tee -a /etc/sysctl.conf

  # Set the active IPv6 kernel parameters
  sysctl -w net.ipv6.conf.all.accept_redirects=0
  sysctl -w net.ipv6.conf.default.accept_redirects=0
  sysctl -w net.ipv6.route.flush=1

  # Check if the IPv6 parameters are set correctly
  if grep -qsP '^[\s]*net\.ipv6\.conf\.all\.accept_redirects[\s]*=[\s]*0[\s]*$' /etc/sysctl.conf /etc/sysctl.d/* && \
     grep -qsP '^[\s]*net\.ipv6\.conf\.default\.accept_redirects[\s]*=[\s]*0[\s]*$' /etc/sysctl.conf /etc/sysctl.d/*; then
    echo "IPv6 parameters set correctly."
  else
    echo "Failed to set IPv6 parameters."
  fi
fi


## covers 504, 505, 506, 507, 508, 509, 510, 511, 512, 513, 514, 515    