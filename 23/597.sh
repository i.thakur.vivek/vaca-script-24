#!/bin/bash

# Set the TMOUT configuration as a variable
TMOUT_CONFIG="TMOUT=900\nreadonly TMOUT\nexport TMOUT\n"

# Choose a filename for the new file
FILENAME="tmout_config.sh"

# Create the new file in /etc/profile.d/
sudo touch /etc/profile.d/$FILENAME

# Add the TMOUT configuration to the new file
echo -e $TMOUT_CONFIG | sudo tee -a /etc/profile.d/$FILENAME > /dev/null

echo "TMOUT configuration added to /etc/profile.d/$FILENAME"
