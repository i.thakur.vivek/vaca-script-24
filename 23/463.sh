#!/bin/bash

# Edit /etc/fstab to add "nosuid" option to /var/tmp
sed -i '/\/var\/tmp/ s/\(^.*\)\(\s\+\)\(\S\+\)\(\s\+\)\(\S\+\)\(\s\+\)\(.*$\)/\1\2\3\4nosuid,\5\6/' /etc/fstab

# Remount /var/tmp with the correct options
mount -o remount,nosuid /var/tmp

# Check if the remount was successful
if [ $? -ne 0 ]; then
  echo "Failed to remount /var/tmp with the 'nosuid' option."
  exit 1
fi

echo "Successfully remounted /var/tmp with the 'nosuid' option."

exit 0
