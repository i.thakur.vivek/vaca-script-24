#!/bin/bash

# Set PASS_MAX_DAYS and PASS_MIN_DAYS in /etc/login.defs
sed -i 's/^PASS_MAX_DAYS.*/PASS_MAX_DAYS 365/' /etc/login.defs
sed -i 's/^PASS_MIN_DAYS.*/PASS_MIN_DAYS 1/' /etc/login.defs

# Set the default password inactivity period to 30 days
useradd -D -f 30
chage --maxdays 365 ec2-user
chage --mindays 1 ec2-user