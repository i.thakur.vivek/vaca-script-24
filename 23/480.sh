#!/bin/bash

# Define the desired message of the day
MOTD="All activities performed on this system will be monitored."

# Backup the existing motd file
cp /etc/motd /etc/motd.bak

# Remove the existing motd file
rm /etc/motd

# Create a new motd file with the desired message
echo "$MOTD" > /etc/motd

# Replace any instances of m, r, s, v or references to the OS platform in the motd file with the desired message
sed -i '/^[mrsiv]/d' /etc/motd

# Set the permissions for the motd file
chmod 644 /etc/motd

echo "The /etc/motd file has been updated according to your site policy."