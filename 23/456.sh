#!/bin/bash

# Check if script is being run as root user
if [ "$EUID" -ne 0 ]; then
  echo "Please run this script as root user."
  exit 1
fi

# Check if /tmp is already configured in /etc/fstab
if grep -q "/tmp" /etc/fstab; then
  # Update /tmp entry in /etc/fstab with appropriate options
  sed -i '/\/tmp/s/^.*$/tmpfs \/tmp tmpfs defaults,rw,nosuid,nodev,noexec,relatime 0 0/' /etc/fstab
else
  # Create a new entry for /tmp in /etc/fstab with appropriate options
  echo "tmpfs /tmp tmpfs defaults,rw,nosuid,nodev,noexec,relatime 0 0" >> /etc/fstab
fi

# Check if systemd tmp.mount file exists
if [ -f /etc/systemd/system/local-fs.target.wants/tmp.mount ]; then
  # Update tmp.mount file with appropriate options
  sed -i 's/Options=.*/Options=noexec,nodev,nosuid/' /etc/systemd/system/local-fs.target.wants/tmp.mount
else
  # Create a new tmp.mount file with appropriate options
  echo "[Mount]
  What=tmpfs
  Where=/tmp
  Type=tmpfs
  Options=noexec,nodev,nosuid" > /etc/systemd/system/local-fs.target.wants/tmp.mount
fi

# Check if /tmp is already mounted
if mount | grep -q "/tmp"; then
  echo "/tmp is already mounted."
else
  # Attempt to mount /tmp with the default options
  mount -t tmpfs tmpfs /tmp
  
  # Check if /tmp is mounted successfully
  if [ $? -eq 0 ]; then
    echo "/tmp mounted successfully."
  else
    # If mounting fails, try to mount with specific options
    mount -t tmpfs -o rw,nosuid,nodev,noexec,relatime tmpfs /tmp
    
    # Check if /tmp is mounted successfully
    if [ $? -eq 0 ]; then
      echo "/tmp mounted successfully with specific options."
    else
      echo "Failed to mount /tmp."
      exit 1
    fi
  fi
fi

# Remount /tmp with the new options
mount -o remount,noexec,nodev,nosuid /tmp


