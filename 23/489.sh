#!/bin/bash

# Check if nfs-utils package is installed
if rpm -q nfs-utils > /dev/null; then
    # Stop the nfs-server service
    systemctl stop nfs-server
    # Mask the nfs-server service
    systemctl mask nfs-server
    echo "The nfs-server service has been stopped and masked."
else
    echo "The nfs-utils package is not installed. No action required."
fi