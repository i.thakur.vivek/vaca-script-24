#!/bin/bash

# Specify the file name that you want to edit or create
filename="/etc/modprobe.d/cramfs.conf"

# Check if the file exists
if [ -f "$filename" ]; then
  echo "The file already exists, editing it..."
else
  echo "The file doesn't exist, creating it..."
  touch $filename
fi

# Add the line to the file
echo "install cramfs /bin/true" >> $filename

# Unload the cramfs module
rmmod cramfs

echo "Done!"
