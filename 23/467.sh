#!/bin/bash

# Check if script is being run as root user
if [ "$EUID" -ne 0 ]; then
  echo "Please run this script as root user."
  exit 1
fi

# Add "install usb-storage /bin/true" to /etc/modprobe.d/blacklist.conf
echo "install usb-storage /bin/true" >> /etc/modprobe.d/blacklist.conf

# Check if usb-storage module is loaded
if ! lsmod | grep -q "^usb_storage "; then
  echo "usb-storage module is not currently loaded."
else
  # Unload the usb-storage module
  rmmod usb-storage
  
  # Check if usb-storage module is unloaded successfully
  if [ $? -eq 0 ]; then
    echo "usb-storage module unloaded successfully."
  else
    echo "Failed to unload usb-storage module."
    exit 1
  fi
fi