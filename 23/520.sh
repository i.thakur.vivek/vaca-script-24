#!/bin/bash

# Set the parameters in the sysctl.conf or sysctl.d/* file
echo "net.ipv4.icmp_echo_ignore_broadcasts = 1" | tee -a /etc/sysctl.conf /etc/sysctl.d/*.conf
echo "net.ipv4.icmp_ignore_bogus_error_responses = 1" | tee -a /etc/sysctl.conf /etc/sysctl.d/*.conf

# Activate the kernel parameters
sysctl -w net.ipv4.icmp_echo_ignore_broadcasts=1
sysctl -w net.ipv4.icmp_ignore_bogus_error_responses=1
sysctl -w net.ipv4.route.flush=1

# Check if the parameters were set correctly
if grep -qs '^net\.ipv4\.icmp_echo_ignore_broadcasts\s*=\s*1' /etc/sysctl.conf /etc/sysctl.d/*.conf && grep -qs '^net\.ipv4\.icmp_ignore_bogus_error_responses\s*=\s*1' /etc/sysctl.conf /etc/sysctl.d/*.conf; then
    echo "Parameters set successfully"
else
    echo "Failed to set parameters"
fi

 ## covers 520, 521