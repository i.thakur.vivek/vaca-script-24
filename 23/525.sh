#!/bin/bash

# Define variables for the parameters we want to set
ALL_ACCEPT_RA="net.ipv6.conf.all.accept_ra"
DEFAULT_ACCEPT_RA="net.ipv6.conf.default.accept_ra"

# Check if IPv6 is enabled
ipv6_enabled=$(sysctl net.ipv6.conf.all.disable_ipv6 | awk '{print $3}')

# If IPv6 is enabled, set the parameters in /etc/sysctl.conf or a /etc/sysctl.d/* file
if [[ $ipv6_enabled -eq 0 ]]; then
  echo "$ALL_ACCEPT_RA = 0" | sudo tee -a /etc/sysctl.conf
  echo "$DEFAULT_ACCEPT_RA = 0" | sudo tee -a /etc/sysctl.conf

  # Reload the sysctl settings
  sudo sysctl -p

  # Set the active kernel parameters
  sudo sysctl -w "$ALL_ACCEPT_RA=0"
  sudo sysctl -w "$DEFAULT_ACCEPT_RA=0"
  sudo sysctl -w net.ipv6.route.flush=1
else
  echo "IPv6 is not enabled on this system"
fi

## 3.3.9 Ensure IPv6 router advertisements are not accepted
## same script for 525, 526, 527, 528 i.e all ipv6 related items ##
