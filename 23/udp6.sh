#!/bin/bash

# Allow incoming traffic on UDP6 port 111
ip6tables -A INPUT -p udp -m udp --dport 111 -j ACCEPT

# Allow incoming traffic on UDP6 port 616
ip6tables -A INPUT -p udp -m udp --dport 616 -j ACCEPT

# Allow incoming traffic on UDP6 port 618
ip6tables -A INPUT -p udp -m udp --dport 618 -j ACCEPT

# Allow incoming traffic on UDP6 port 546
ip6tables -A INPUT -p udp -m udp --dport 546 -j ACCEPT
