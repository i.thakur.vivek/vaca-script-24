
# If there are no security updates available, exit with success
yum update -y
if [[ $security_updates -eq 0 ]]; then
  echo "No security updates available."
  exit 0
fi

# If there are security updates available, exit with failure
echo "Security updates available."
exit 1
