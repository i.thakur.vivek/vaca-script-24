#!/bin/bash

# backup the original file
cp /etc/pam.d/system-auth /etc/pam.d/system-auth.bak

# replace the contents of the file with the new configuration
cat <<EOT > /etc/pam.d/password-auth
#%PAM-1.0
# This file is auto-generated.
# User changes will be destroyed the next time authconfig is run.
auth        required      pam_env.so
auth        required      pam_tally2.so deny=5 onerr=fail unlock_time=900
auth        required      pam_faillock.so preauth silent audit deny=5 unlock_time=900
auth        sufficient    pam_unix.so try_first_pass nullok
auth        [default=die] pam_faillock.so authfail audit deny=5 unlock_time=900
auth        required      pam_deny.so

account     required      pam_faillock.so
account     required      pam_tally2.so
account     required      pam_unix.so

password    requisite     pam_pwquality.so try_first_pass local_users_only retry=3 authtok_type=
password    required      pam_pwhistory.so use_authtok remember=5 retry=3
password    sufficient    pam_unix.so try_first_pass use_authtok nullok sha512 shadow
password    required      pam_deny.so

session     optional      pam_keyinit.so revoke
session     required      pam_limits.so
-session     optional      pam_systemd.so
session     [success=1 default=ignore] pam_succeed_if.so service in crond quiet use_uid
session     required      pam_unix.so
EOT

# print a message to confirm the update
echo "The /etc/pam.d/password-auth file has been updated."
