#!/bin/bash

# Define variables for the parameters we want to set
ALL_RP_FILTER="net.ipv4.conf.all.rp_filter"
DEFAULT_RP_FILTER="net.ipv4.conf.default.rp_filter"

# Set the parameters in /etc/sysctl.conf or a /etc/sysctl.d/* file
echo "$ALL_RP_FILTER = 1" | sudo tee -a /etc/sysctl.conf
echo "$DEFAULT_RP_FILTER = 1" | sudo tee -a /etc/sysctl.conf

# Reload the sysctl settings
sudo sysctl -p

# Set the active kernel parameters
sudo sysctl -w "$ALL_RP_FILTER=1"
sudo sysctl -w "$DEFAULT_RP_FILTER=1"
sudo sysctl -w net.ipv4.route.flush=1

##Ensure Reverse Path Filtering is enabled
## covers 522, 523 