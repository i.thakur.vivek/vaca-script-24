#!/bin/bash

# This script removes the X Windows Server packages

if ! command -v yum &> /dev/null
then
    echo "yum could not be found, please make sure it is installed"
    exit 1
fi

echo "Removing X Windows Server packages..."
sudo yum remove -y xorg-x11-server*
if [ $? -eq 0 ]
then
    echo "X Windows Server packages have been successfully removed"
else
    echo "Failed to remove X Windows Server packages"
fi