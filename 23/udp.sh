#!/bin/bash

# Allow incoming traffic on UDP port 68
iptables -A INPUT -p udp --dport 68 -j ACCEPT

# Allow incoming traffic on UDP port 111
iptables -A INPUT -p udp --dport 111 -j ACCEPT

# Allow incoming traffic on UDP port 618
iptables -A INPUT -p udp --dport 618 -j ACCEPT

# Allow incoming traffic on UDP port 616
iptables -A INPUT -p udp --dport 616 -j ACCEPT
