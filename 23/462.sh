#!/bin/bash

# Edit /etc/fstab to add "nodev" option to /var/tmp
sed -i '/\/var\/tmp/ s/\(^.*\)\(\s\+\)\(\S\+\)\(\s\+\)\(\S\+\)\(\s\+\)\(.*$\)/\1\2\3\4nodev,\5\6/' /etc/fstab

# Check if the mount point is currently mounted
if ! mountpoint -q /var/tmp; then
  echo "/var/tmp is not currently mounted."
  exit 1
fi

# Check if the mount options are correct
if ! mount | grep -q "/var/tmp.*nodev"; then
  echo "/var/tmp is not mounted with the 'nodev' option. Remounting with 'nodev' option..."

  # Remount /var/tmp with the correct options
  mount -o remount,nodev /var/tmp

  # Check if the remount was successful
  if [ $? -ne 0 ]; then
    echo "Failed to remount /var/tmp with the 'nodev' option."
    exit 1
  fi

  echo "Successfully remounted /var/tmp with the 'nodev' option."
else
  echo "/var/tmp is already mounted with the 'nodev' option."
fi

exit 0
