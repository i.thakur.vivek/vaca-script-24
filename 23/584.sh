#!/bin/bash

# Edit pwquality.conf
sed -i 's/# minlen =.*/minlen = 14/' /etc/security/pwquality.conf
sed -i 's/# minclass =.*/minclass = 4/' /etc/security/pwquality.conf
sed -i 's/# dcredit =.*/dcredit = -1/' /etc/security/pwquality.conf
sed -i 's/# ucredit =.*/ucredit = -1/' /etc/security/pwquality.conf
sed -i 's/# ocredit =.*/ocredit = -1/' /etc/security/pwquality.conf
sed -i 's/# lcredit =.*/lcredit = -1/' /etc/security/pwquality.conf


#covers 584 to 588 