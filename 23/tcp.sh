#!/bin/bash

# Allow incoming traffic on TCP port 22
iptables -A INPUT -p tcp --dport 22 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 22 -j ACCEPT
iptables -A FORWARD -p tcp --dport 22 -j ACCEPT

# Allow incoming traffic on TCP ports 30000-32767
iptables -A INPUT -p tcp --dport 30000:32767 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 30000:32767 -j ACCEPT
iptables -A FORWARD -p tcp --dport 30000:32767 -j ACCEPT

# Allow incoming traffic on TCP port 111
iptables -A INPUT -p tcp --dport 111 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 111 -j ACCEPT
iptables -A FORWARD -p tcp --dport 111 -j ACCEPT