#!/bin/bash

# Check if the script is being run as root
if [ $(id -u) -ne 0 ]; then
    echo "This script must be run as root."
    exit 1
fi

# Mask the rsyncd service
echo "Masking the rsyncd service."
systemctl --now mask rsyncd

# Print a message to indicate the completion of the script
echo "The rsyncd service has been masked successfully."