#!/bin/bash

# Allow incoming traffic on TCP6 port 22
iptables -A INPUT -p tcp --dport 22 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 22 -j ACCEPT
iptables -A FORWARD -p tcp --dport 22 -j ACCEPT

# Allow incoming traffic on TCP6 ports 30000-32767
iptables -A INPUT -p tcp --dport 30000:32767 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 30000:32767 -j ACCEPT
iptables -A FORWARD -p tcp --dport 30000:32767 -j ACCEPT

# Allow incoming traffic on TCP6 port 10249
iptables -A INPUT -p tcp --dport 10249 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 10249 -j ACCEPT
iptables -A FORWARD -p tcp --dport 10249 -j ACCEPT

# Allow incoming traffic on TCP6 port 10250
iptables -A INPUT -p tcp --dport 10250 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 10250 -j ACCEPT
iptables -A FORWARD -p tcp --dport 10250 -j ACCEPT

# Allow incoming traffic on TCP6 port 9100
iptables -A INPUT -p tcp --dport 9100 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 9100 -j ACCEPT
iptables -A FORWARD -p tcp --dport 9100 -j ACCEPT

# Allow incoming traffic on TCP6 port 61678
iptables -A INPUT -p tcp --dport 61678 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 61678 -j ACCEPT
iptables -A FORWARD -p tcp --dport 61678 -j ACCEPT

# Allow incoming traffic on TCP6 port 111
iptables -A INPUT -p tcp --dport 111 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 111 -j ACCEPT
iptables -A FORWARD -p tcp --dport 111 -j ACCEPT

# Allow incoming traffic on TCP6 port 10256
iptables -A INPUT -p tcp --dport 10256 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 10256 -j ACCEPT
iptables -A FORWARD -p tcp --dport 10256 -j ACCEPT

# Allow incoming traffic on TCP6 port 9809
iptables -A INPUT -p tcp --dport 9809 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 9809 -j ACCEPT
iptables -A FORWARD -p tcp --dport 9809 -j ACCEPT

# Allow incoming traffic on TCP6 port 5473
iptables -A INPUT -p tcp --dport 5473 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 5473 -j ACCEPT
iptables -A FORWARD -p tcp --dport 5473 -j ACCEPT

# Allow incoming traffic on TCP6 port 111
iptables -A INPUT -p tcp --dport 111 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 111 -j ACCEPT
iptables -A FORWARD -p tcp --dport 111 -j ACCEPT
iptables -A FORWARD -p tcp --sport 111 -j ACCEPT

# Allow incoming traffic on TCP6 port 10256
iptables -A INPUT -p tcp --dport 10256 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 10256 -j ACCEPT
iptables -A FORWARD -p tcp --dport 10256 -j ACCEPT
iptables -A FORWARD -p tcp --sport 10256 -j ACCEPT

# Allow incoming traffic on TCP6 port 9809
iptables -A INPUT -p tcp --dport 9809 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 9809 -j ACCEPT
iptables -A FORWARD -p tcp --dport 9809 -j ACCEPT
iptables -A FORWARD -p tcp --sport 9809 -j ACCEPT

# Allow incoming traffic on TCP6 port 5473
iptables -A INPUT -p tcp --dport 5473 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 5473 -j ACCEPT
iptables -A FORWARD -p tcp --dport 5473 -j ACCEPT
iptables -A FORWARD -p tcp --sport 5473 -j ACCEPT


# Allow incoming traffic on TCP6 port 443
iptables -A INPUT -p tcp --dport 443 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 443 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 443 -j ACCEPT
iptables -A FORWARD -p tcp --dport 443 -j ACCEPT
iptables -A FORWARD -p tcp --sport 443 -j ACCEPT

# Allow incoming traffic on TCP6 port 6443
iptables -A INPUT -p tcp --dport 6443 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 6443 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 6443 -j ACCEPT
iptables -A FORWARD -p tcp --dport 6443 -j ACCEPT
iptables -A FORWARD -p tcp --sport 6443 -j ACCEPT

# Allow incoming traffic on TCP6 port 10251
iptables -A INPUT -p tcp --dport 10251 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 10251 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 10251 -j ACCEPT
iptables -A FORWARD -p tcp --dport 10251 -j ACCEPT

# Allow incoming traffic on TCP6 port 10252
iptables -A INPUT -p tcp --dport 10252 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 10252 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 10252 -j ACCEPT
iptables -A FORWARD -p tcp --dport 10252 -j ACCEPT


#ipv6

# Allow incoming traffic on TCP6 port 22
ip6tables -A INPUT -p tcp --dport 22 -j ACCEPT
ip6tables -A OUTPUT -p tcp --sport 22 -j ACCEPT
ip6tables -A FORWARD -p tcp --dport 22 -j ACCEPT

# Allow incoming traffic on TCP6 ports 30000-32767
ip6tables -A INPUT -p tcp --dport 30000:32767 -j ACCEPT
ip6tables -A OUTPUT -p tcp --sport 30000:32767 -j ACCEPT
ip6tables -A FORWARD -p tcp --dport 30000:32767 -j ACCEPT

# Allow incoming traffic on TCP6 port 10249
ip6tables -A INPUT -p tcp --dport 10249 -j ACCEPT
ip6tables -A OUTPUT -p tcp --sport 10249 -j ACCEPT
ip6tables -A FORWARD -p tcp --dport 10249 -j ACCEPT

# Allow incoming traffic on TCP6 port 10250
ip6tables -A INPUT -p tcp --dport 10250 -j ACCEPT
ip6tables -A OUTPUT -p tcp --sport 10250 -j ACCEPT
ip6tables -A FORWARD -p tcp --dport 10250 -j ACCEPT

# Allow incoming traffic on TCP6 port 9100
ip6tables -A INPUT -p tcp --dport 9100 -j ACCEPT
ip6tables -A OUTPUT -p tcp --sport 9100 -j ACCEPT
ip6tables -A FORWARD -p tcp --dport 9100 -j ACCEPT

# Allow incoming traffic on TCP6 port 61678
ip6tables -A INPUT -p tcp --dport 61678 -j ACCEPT
ip6tables -A OUTPUT -p tcp --sport 61678 -j ACCEPT
ip6tables -A FORWARD -p tcp --dport 61678 -j ACCEPT

# Allow incoming traffic on TCP6 port 111
ip6tables -A INPUT -p tcp --dport 111 -j ACCEPT
ip6tables -A OUTPUT -p tcp --sport 111 -j ACCEPT
ip6tables -A FORWARD -p tcp --dport 111 -j ACCEPT

# Allow incoming traffic on TCP6 port 10256
ip6tables -A INPUT -p tcp --dport 10256 -j ACCEPT
ip6tables -A OUTPUT -p tcp --sport 10256 -j ACCEPT
ip6tables -A FORWARD -p tcp --dport 10256 -j ACCEPT

# Allow incoming traffic on TCP6 port 9809
ip6tables -A INPUT -p tcp --dport 9809 -j ACCEPT
ip6tables -A OUTPUT -p tcp --sport 9809 -j ACCEPT
ip6tables -A FORWARD -p tcp --dport 9809 -j ACCEPT

# Allow incoming traffic on TCP6 port 5473
ip6tables -A INPUT -p tcp --dport 5473 -j ACCEPT
ip6tables -A OUTPUT -p tcp --sport 5473 -j ACCEPT
ip6tables -A FORWARD -p tcp --dport 5473 -j ACCEPT

# Allow incoming traffic on TCP6 port 111
ip6tables -A INPUT -p tcp --dport 111 -j ACCEPT
ip6tables -A OUTPUT -p tcp --sport 111 -j ACCEPT
ip6tables -A FORWARD -p tcp --dport 111 -j ACCEPT
ip6tables -A FORWARD -p tcp --sport 111 -j ACCEPT

# Allow incoming traffic on TCP6 port 10256
ip6tables -A INPUT -p tcp --dport 10256 -j ACCEPT
ip6tables -A OUTPUT -p tcp --sport 10256 -j ACCEPT
ip6tables -A FORWARD -p tcp --dport 10256 -j ACCEPT
ip6tables -A FORWARD -p tcp --sport 10256 -j ACCEPT

# Allow incoming traffic on TCP6 port 9809
ip6tables -A INPUT -p tcp --dport 9809 -j ACCEPT
ip6tables -A OUTPUT -p tcp --sport 9809 -j ACCEPT
ip6tables -A FORWARD -p tcp --dport 9809 -j ACCEPT
ip6tables -A FORWARD -p tcp --sport 9809 -j ACCEPT

# Allow incoming traffic on TCP6 port 5473
ip6tables -A INPUT -p tcp --dport 5473 -j ACCEPT
ip6tables -A OUTPUT -p tcp --sport 5473 -j ACCEPT
ip6tables -A FORWARD -p tcp --dport 5473 -j ACCEPT
ip6tables -A FORWARD -p tcp --sport 5473 -j ACCEPT
