#!/bin/bash

# Check if script is being run as root user
if [ "$EUID" -ne 0 ]; then
  echo "Please run this script as root user."
  exit 1
fi

# Check if /var/tmp is configured in /etc/fstab
if grep -q "/var/tmp" /etc/fstab; then
  # Update /var/tmp entry in /etc/fstab with "noexec" option
  sed -i 's/\(.*\/var\/tmp.*\)\(defaults\)\(.*\)/\1\2,noexec\3/' /etc/fstab
fi

# Check if /var/tmp is already mounted
if mount | grep -q "/var/tmp"; then
  # If /var/tmp is already mounted, exit with a message
  echo "/var/tmp is already mounted."
else
  # If /var/tmp is not mounted, mount it with defaults and noexec option
  mount -t tmpfs -o defaults,noexec tmpfs /var/tmp
  
  # Check if /var/tmp is mounted successfully
  if [ $? -eq 0 ]; then
    echo "/var/tmp mounted successfully with 'noexec' option."
  else
    echo "Failed to mount /var/tmp."
    exit 1
  fi
fi


