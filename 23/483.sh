#!/bin/bash

echo "Authorized uses only. All activity may be monitored and reported." > /etc/issue.net
sed -i 's/\\[mrsv]//g' /etc/issue.net

echo "Changes to /etc/issue.net have been made:"
cat /etc/issue.net

#covers both 483 and 484