#!/bin/bash

# Set LogLevel to INFO
sed -i 's/#LogLevel INFO/LogLevel INFO/g' /etc/ssh/sshd_config

# Set MaxAuthTries to 4
sed -i 's/#MaxAuthTries 6/MaxAuthTries 4/g' /etc/ssh/sshd_config

# Set IgnoreRhosts to yes
sed -i 's/#IgnoreRhosts yes/IgnoreRhosts yes/g' /etc/ssh/sshd_config

# Set HostbasedAuthentication to no
sed -i 's/#HostbasedAuthentication no/HostbasedAuthentication no/g' /etc/ssh/sshd_config

# Set PermitRootLogin to no
sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin no/g' /etc/ssh/sshd_config
sed -i 's/#PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config

# Set PermitEmptyPasswords to no
sed -i 's/#PermitEmptyPasswords no/PermitEmptyPasswords no/g' /etc/ssh/sshd_config

# Set PermitUserEnvironment to no
sed -i 's/#PermitUserEnvironment no/PermitUserEnvironment no/g' /etc/ssh/sshd_config

# Add site-approved ciphers
echo "Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr" >> /etc/ssh/sshd_config

# Add site-approved MACs
echo "MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-sha2-512,hmac-sha2-256" >> /etc/ssh/sshd_config

# Add site-approved key exchange algorithms
echo "KexAlgorithms curve25519-sha256,curve25519-sha256@libssh.org,ecdh-sha2-nistp521,ecdh-sha2-nistp384,ecdh-sha2-nistp256,diffie-hellman-group-exchange-sha256" >> /etc/ssh/sshd_config

# Set LoginGraceTime to 60 seconds
sed -i 's/#LoginGraceTime 2m/LoginGraceTime 60/g' /etc/ssh/sshd_config

# Set Banner to /etc/issue.net
sed -i 's/#Banner none/Banner \/etc\/issue.net/g' /etc/ssh/sshd_config

# Set MaxStartups to 10:30:60
sed -i 's/#MaxStartups .*/MaxStartups 10:30:60/g' /etc/ssh/sshd_config

# Set MaxSessions to 10
echo "MaxSessions 10" >> /etc/ssh/sshd_config

# Set the SSH idle timeout interval parameters
sed -i 's/#ClientAliveInterval 0/ClientAliveInterval 900/g' /etc/ssh/sshd_config
sed -i 's/#ClientAliveCountMax 3/ClientAliveCountMax 0/g' /etc/ssh/sshd_config

# Restart the sshd service to apply the changes
systemctl restart sshd.service

## covers items (562 to 577) and (580 to 583)