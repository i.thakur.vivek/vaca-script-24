#!/bin/bash

# Define variables for the parameter we want to set
TCP_SYN_COOKIES="net.ipv4.tcp_syncookies"

# Set the parameter in /etc/sysctl.conf or a /etc/sysctl.d/* file
echo "$TCP_SYN_COOKIES = 1" | sudo tee -a /etc/sysctl.conf

# Reload the sysctl settings
sudo sysctl -p

# Set the active kernel parameters
sudo sysctl -w "$TCP_SYN_COOKIES=1"
sudo sysctl -w net.ipv4.route.flush=1

##Ensure TCP SYN Cookies is enabled