#!/bin/bash

# Check if the script is being run as root
if [ $(id -u) -ne 0 ]; then
    echo "This script must be run as root."
    exit 1
fi

# Check if the rpcbind package is installed
if ! rpm -q rpcbind > /dev/null; then
    echo "The rpcbind package is not installed."
    exit 1
fi

# Mask the rpcbind package and services
echo "Masking the rpcbind and rpcbind.socket services."
systemctl --now mask rpcbind
systemctl --now mask rpcbind.socket

# Print a message to indicate the completion of the script
echo "The rpcbind package and services have been masked successfully."