#!/bin/bash



# Allow all traffic on the loopback interface
ip6tables -A INPUT -i lo -j ACCEPT
ip6tables -A OUTPUT -o lo -j ACCEPT

# Block all traffic from the ::1 address
ip6tables -A INPUT -s ::1 -j DROP

# Drop all traffic from the loopback interface in the OUTPUT chain
ip6tables -A OUTPUT ! -o lo -d ::1 -j DROP
ip6tables -A OUTPUT -s localhost6 -o lo -j DROP

# Save the rules to the configuration file so they persist after a reboot
ip6tables-save > /etc/sysconfig/ip6tables
systemctl enable ip6tables
systemctl --now start ip6tables


## includes 540, 541, 542, 543, 544
