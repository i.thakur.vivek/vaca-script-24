#!/bin/bash

# Set the desired file permission mode
file_create_mode="0640"

# Check if $FileCreateMode is already set in /etc/rsyslog.conf
if ! grep -q "^\\\$FileCreateMode" /etc/rsyslog.conf; then
    echo "\$FileCreateMode $file_create_mode" >> /etc/rsyslog.conf
else
    sed -i "s/^\\\$FileCreateMode.*/\\\$FileCreateMode $file_create_mode/" /etc/rsyslog.conf
fi

# Check if $FileCreateMode is already set in /etc/rsyslog.d/*.conf files
for conf_file in /etc/rsyslog.d/*.conf; do
    if ! grep -q "^\\\$FileCreateMode" "$conf_file"; then
        echo "\$FileCreateMode $file_create_mode" >> "$conf_file"
    else
        sed -i "s/^\\\$FileCreateMode.*/\\\$FileCreateMode $file_create_mode/" "$conf_file"
    fi
done

# Restart rsyslog to apply the changes
systemctl restart rsyslog

## covers 550