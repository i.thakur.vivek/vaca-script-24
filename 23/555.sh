#!/bin/bash

# Check if the Compress, Storage, and ForwardToSyslog lines are already present in the journald.conf file
if grep -q "^Compress=yes" /etc/systemd/journald.conf && grep -q "^Storage=persistent" /etc/systemd/journald.conf && grep -q "^ForwardToSyslog=yes" /etc/systemd/journald.conf; then
    echo "Compress, Storage, and ForwardToSyslog are already enabled in journald.conf"
    exit 0
fi

# Add the Compress, Storage, and ForwardToSyslog lines to the journald.conf file
echo "Compress=yes" >> /etc/systemd/journald.conf
echo "Storage=persistent" >> /etc/systemd/journald.conf
echo "ForwardToSyslog=yes" >> /etc/systemd/journald.conf

# Restart the systemd-journald service
systemctl restart systemd-journald

# Check if the Compress, Storage, and ForwardToSyslog lines were added successfully
if grep -q "^Compress=yes" /etc/systemd/journald.conf && grep -q "^Storage=persistent" /etc/systemd/journald.conf && grep -q "^ForwardToSyslog=yes" /etc/systemd/journald.conf; then
    echo "Compress, Storage, and ForwardToSyslog enabled in journald.conf"
else
    echo "Failed to enable Compress, Storage, and ForwardToSyslog in journald.conf"
    exit 1
fi
## covers 555, 556, 557