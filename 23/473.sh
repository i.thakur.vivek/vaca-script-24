#!/bin/bash

# Add the following line to /etc/security/limits.conf or a /etc/security/limits.d/* file
echo "* hard core 0" | sudo tee -a /etc/security/limits.conf

# Set the following parameter in /etc/sysctl.conf or a /etc/sysctl.d/* file
echo "fs.suid_dumpable = 0" | sudo tee -a /etc/sysctl.conf

# Run the following command to set the active kernel parameter
sudo sysctl -w fs.suid_dumpable=0

# If systemd-coredump is installed, edit /etc/systemd/coredump.conf and add/modify the following lines
if [ -f "/etc/systemd/coredump.conf" ]; then
    sudo sed -i 's/^#Storage=.*$/Storage=none/' /etc/systemd/coredump.conf
    sudo sed -i 's/^#ProcessSizeMax=.*$/ProcessSizeMax=0/' /etc/systemd/coredump.conf
fi

# Run the command to reload the systemd daemon
sudo systemctl daemon-reload

#covers 473 and 474
