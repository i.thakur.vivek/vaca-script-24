#!/bin/bash

# Check if the script is being run as root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

# Edit the /etc/fstab file
echo "tmpfs      /dev/shm    tmpfs   defaults,noexec,nodev,nosuid,seclabel   0 0" >> /etc/fstab

# Remount /dev/shm with the specified options
mount -o remount,noexec,nodev,nosuid /dev/shm