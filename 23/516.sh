#!/bin/bash

# Set the parameters in the sysctl.conf or sysctl.d/* file
echo "net.ipv4.conf.all.log_martians = 1" | tee -a /etc/sysctl.conf /etc/sysctl.d/*.conf
echo "net.ipv4.conf.default.log_martians = 1" | tee -a /etc/sysctl.conf /etc/sysctl.d/*.conf

# Activate the kernel parameters
sysctl -w net.ipv4.conf.all.log_martians=1
sysctl -w net.ipv4.conf.default.log_martians=1
sysctl -w net.ipv4.route.flush=1

# Check if the parameters were set correctly
/bin/grep -s -P '^[\s]*net\.ipv4\.conf\.all\.log_martians[\s]*=[\s]*1[\s]*$' /etc/sysctl.conf /etc/sysctl.d/* | /bin/awk '{print} END {if (NR != 0) print "pass" ; else print "fail"}'

##covers 516, 517, 518, 519,  