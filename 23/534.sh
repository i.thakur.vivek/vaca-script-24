#!/bin/bash

# Get the open IPv4 TCP and UDP ports from netstat output
open_tcp_ports=$(netstat -tunl | awk '/^tcp/ {if ($4 !~ /127.0.0.1/) {split($4,a,":"); print a[2]}}')
open_udp_ports=$(netstat -tunl | awk '/^udp/ {if ($4 !~ /127.0.0.1/) {split($4,a,":"); print a[2]}}')

# Get the open IPv6 TCP and UDP ports from netstat output
open_tcp6_ports=$(netstat -tunl | awk '/^tcp6/ {if ($4 !~ /::1/) {split($4,a,"\\[\\]:"); print a[2]}}')
open_udp6_ports=$(netstat -tunl | awk '/^udp6/ {if ($4 !~ /::1/) {split($4,a,"\\[\\]:"); print a[2]}}')


# Apply iptables rule for each open IPv4 TCP port
for tcp_port in $open_tcp_ports; do
  if [[ $tcp_port =~ ^[0-9]+$ ]]; then
    iptables -A INPUT -p tcp --dport $tcp_port -m state --state NEW -j ACCEPT
  fi
done

# Apply iptables rule for each open IPv4 UDP port
for udp_port in $open_udp_ports; do
  if [[ $udp_port =~ ^[0-9]+$ ]]; then
    iptables -A INPUT -p udp --dport $udp_port -m state --state NEW -j ACCEPT
  fi
done

# Apply iptables rule for each open IPv6 TCP port
for tcp6_port in $open_tcp6_ports; do
  if [[ $tcp6_port =~ ^[0-9]+$ ]]; then
    ip6tables -A INPUT -p tcp --dport $tcp6_port -m state --state NEW -j ACCEPT
  fi
done

# Apply iptables rule for each open IPv6 UDP port
for udp6_port in $open_udp6_ports; do
  if [[ $udp6_port =~ ^[0-9]+$ ]]; then
    ip6tables -A INPUT -p udp --dport $udp6_port -m state --state NEW -j ACCEPT
  fi
done

