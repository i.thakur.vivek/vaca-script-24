#!/bin/bash


# Save the rules to the configuration file so they persist after a reboot
ip6tables-save > /etc/sysconfig/ip6tables
systemctl enable ip6tables
systemctl --now start ip6tables
