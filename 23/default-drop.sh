#!/bin/bash

# Set default DROP policy
iptables -P INPUT DROP
iptables -P OUTPUT DROP
iptables -P FORWARD DROP