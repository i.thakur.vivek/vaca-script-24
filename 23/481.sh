#!/bin/bash

# This script sets the contents of the /etc/issue file to the specified warning message,
# and removes any instances of m, r, s, v or references to the OS platform from the file.

# Set the warning message
WARNING_MSG="Authorized uses only. All activity may be monitored and reported."

# Remove any instances of m, r, s, v or references to the OS platform
sed -i 's/\\[mrsv]//g' /etc/issue

# Replace the contents of the file with the warning message
echo -e "$WARNING_MSG" > /etc/issue

#covers 481 and 482