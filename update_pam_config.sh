#!/bin/bash

# Backup the original files
cp /etc/pam.d/system-auth /etc/pam.d/system-auth.bak
cp /etc/pam.d/password-auth /etc/pam.d/password-auth.bak

# Function to update PAM configuration with pam_faillock.so
update_with_pam_faillock() {
    local PAM_FILE=$1

    sed -i '/^auth\s\+required\s\+pam_env.so/a auth        required      pam_faillock.so preauth silent audit deny=5 unlock_time=900' $PAM_FILE
    sed -i '/^auth\s\+sufficient\s\+pam_unix.so/a auth        [default=die] pam_faillock.so authfail audit deny=5 unlock_time=900' $PAM_FILE
    sed -i '/^account\s\+required\s\+pam_unix.so/i account     required      pam_faillock.so' $PAM_FILE
}

# Update system-auth
echo "Updating /etc/pam.d/system-auth"
update_with_pam_faillock /etc/pam.d/system-auth

# Update password-auth
echo "Updating /etc/pam.d/password-auth"
update_with_pam_faillock /etc/pam.d/password-auth

# Notify user
echo "Update completed. Please review the changes in /etc/pam.d/system-auth and /etc/pam.d/password-auth."
echo "Original files have been backed up as /etc/pam.d/system-auth.bak and /etc/pam.d/password-auth.bak."
echo "Test the login process to ensure the new PAM configurations are working as expected."
