#!/bin/bash

find / -xdev \( -perm -4000 -o -perm -2000 \) -type f | awk '{print "-a always,exit -F path=" $1 " -F perm=x -F auid>='"$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)"' -F auid!=4294967295 -k privileged" }' >> /etc/audit/rules.d/50-privileged.rules

# Reminder for manual service restart to prevent unwanted disruptions.
# Restart auditd service to apply changes
echo "Restarting auditd service..."
sudo service auditd restart

# Check for rules to confirm
echo "Current audit rules for '-k privileged"
/sbin/auditctl -l | /bin/grep -w "privileged"
