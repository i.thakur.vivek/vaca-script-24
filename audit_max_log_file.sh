#!/bin/bash

# Function to update or append parameter in the auditd configuration file
update_auditd_conf() {
    local CONF_FILE="/etc/audit/auditd.conf"
    local PARAM="$1"
    local VALUE="$2"

    # Backup the original file before making changes
    cp "$CONF_FILE" "${CONF_FILE}.bak"

    # Check if the parameter exists in the file
    if grep -q "^${PARAM} = " "$CONF_FILE"; then
        # Parameter exists, update its value
        sed -i "s/^${PARAM} = .*/${PARAM} = ${VALUE}/" "$CONF_FILE"
        echo "Updated $PARAM to $VALUE in $CONF_FILE."
    else
        # Parameter does not exist, append it to the file
        echo "$PARAM = $VALUE" >> "$CONF_FILE"
        echo "Appended $PARAM = $VALUE to $CONF_FILE."
    fi

    service auditd restart
}

# Call function to update or append parameters for max_log_file_action and max_log_file
update_auditd_conf "max_log_file_action" "keep_logs"
update_auditd_conf "max_log_file" "8"
