#!/bin/bash

# Path to the SELINUX configuration file
SELINUX_CONFIG="/etc/selinux/config"

# Backup the original SELINUX configuration file
cp ${SELINUX_CONFIG} ${SELINUX_CONFIG}.bak

# Check if SELINUX directive exists
if grep -q "^SELINUX=" ${SELINUX_CONFIG}; then
    # SELINUX directive exists, modify its value to enforcing
    sed -i 's/^SELINUX=.*/SELINUX=enforcing/' ${SELINUX_CONFIG}
else
    # AllowTcpForwarding directive does not exist, add it to the file
    echo "SELINUX=enforcing" >> ${SELINUX_CONFIG}
fi

echo "SELINUX enforcing is enable"
